= PuraVida Asciidoctor Extensions

Welcome to the Puravida Asciidoctor extensions site. 

include::_fragment/extensions.adoc[]
include::_fragment/themes.adoc[]
include::_fragment/barcode.adoc[]
include::_fragment/quizzes.adoc[]
include::_fragment/plot.adoc[]

